package com.itconsortiumgh.vodafone.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

@Component
public class SOAPMessagePoster {
//	static {
////	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
////	        {
////
////	            public boolean verify(String hostname, SSLSession session)
////	            {
//		System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1");
//
////	                if (hostname.equals("10.179.100.244"))
////	                    return true;
////	                return false;
////	            }
////	        });
//	}
	
	Logger log = LoggerFactory.getLogger(getClass());

	public String postSoapMessage(boolean verifyHostname, String endpointURL, String soapMessage) throws TimeoutException,IOException{
		OkHttpClient client = new OkHttpClient();
		System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1");
		
		if(verifyHostname==false) {
		client.setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		}
		MediaType mediaType = MediaType.parse("application/xml");
		RequestBody body = RequestBody.create(mediaType, soapMessage);
		Request request = new Request.Builder()
				.url(endpointURL)
				.post(body)
				.addHeader("content-type", "application/xml")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c9de9d1c-6191-bd87-74d9-bf6228414695")
				.build();
		java.lang.System.setProperty("https.protocols", "TLSv1.2");

		String response = client.newCall(request).execute().body().string();
		return response;
	}
	
	public String postSoapMessageIgnoreSSL(String endpointURL, String soapMessage) throws IOException{
		OkHttpClient client = new OkHttpClient();
		
		MediaType mediaType = MediaType.parse("application/xml");
		RequestBody body = RequestBody.create(mediaType, soapMessage);
		Request request = new Request.Builder()
				.url(endpointURL)
				.post(body)
				.addHeader("content-type", "application/xml")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c9de9d1c-6191-bd87-74d9-bf6228414695")
				.build();
		

		String response = client.newCall(request).execute().body().string();
		return response;
	}

	public String sendMessage(String endpointURL, String soapMessage){
		String result = "";
		BufferedReader in = null;
		OutputStream out = null;
		//        // Create the connection where we're going to send the file.
		URL url =null;
		try {
			url = new URL(endpointURL);

			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;

			// Open the input file. After we copy it to a byte array, we can see
			// how big it is so that we can set the HTTP Cotent-Length
			// property. (See complete e-mail below for more on this.)

			byte[] b = soapMessage.getBytes();
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty( "Content-Length",
					String.valueOf( b.length ) );
			httpConn.setRequestProperty("Content-Type","text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction","");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML that was read in to b.
			out = httpConn.getOutputStream();
			out.write(b);    
			out.flush();

			// Read the response and write it to standard out.

			InputStream _is = null;
			if (httpConn.getResponseCode() >= 400) {
				_is = httpConn.getErrorStream(); 
			} else { 
				/* error from server */ 
				_is = httpConn.getInputStream(); 
			} 
			InputStreamReader isr = new InputStreamReader(_is);
			in = new BufferedReader(isr);
			String inputLine;

			while ((inputLine = in.readLine()) != null)
				result = result.concat(inputLine);
			//            System.out.println(inputLine);
			//        return result;
		}catch(MalformedURLException e){
			log.error("malformedURL exception");
			result = e.getMessage();
		} catch (IOException e) {
			log.error("IOException");
			result = e.getMessage();
		}finally{
			try {
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}

			} catch (IOException e) {
				log.error("IOException inside finally clause");
				result = e.getMessage();
			}
		}
		return result;
	}

		public String curl1(String wsdl, String data){
		
		Runtime r = Runtime.getRuntime();
		Process p;
		String messageString = "";
		byte[] messageByte = new byte[1000];
		try {
			String str = "curl -k -X POST -H \"Content-Type: text/xml; \" -H \"Cache-Control: no-cache\" -H \"Postman-Token: 7b735738-267d-935a-fe5f-5f2dd8c97b77\" -d '"+data+"' \""+wsdl+"\"";
			p = r.exec(str);
			
			log.info("curl requests {}", str);
			p.waitFor();
			DataInputStream in = new DataInputStream(p.getInputStream());
			int bytesRead = 0;

			messageByte[0] = in.readByte();
			messageByte[1] = in.readByte();
			int bytesToRead = messageByte[0];
			bytesRead = in.read(messageByte);
			messageString += new String(messageByte, 0, bytesRead);
			System.out.println("---This is the response---"+messageString);
			
//			String l = "";

//			while ((l = b.readLine()) != null) {
//				log.info("curl response {}", l);
//				line.append(l);
//			}
//			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return messageString;
	}

	public String curl(String wsdl, String data){
			String url = "http://localhost/curl.php?url="+wsdl;
			String result = "";
			try {
				result = postSoapMessageIgnoreSSL(url, data);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
	}
		
	  public String curl2(String endpointURL, String soapMessage) {
		
	       String[] command = {"curl", "-X", "POST","-H", "Content-Type: text/xml; ","-H", "Cache-Control: no-cache", "-d", soapMessage, endpointURL};
	       ProcessBuilder process = new ProcessBuilder(command); 
	       Process p;
	       String result = "";
	       try
	       {
	            p = process.start();
	             BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
	                StringBuilder builder = new StringBuilder();
	                String line = null;
	                while ( (line = reader.readLine()) != null) {
	                	System.out.println("line: "+line);
	                        builder.append(line);
	                        builder.append(System.getProperty("line.separator"));
	                }
	                result = builder.toString();
	                System.out.print(result);

	        }
	        catch (IOException e)
	        {   System.out.print("error");
	            e.printStackTrace();
	        }
	        return result;
	}

	

}

