package com.itconsortiumgh.vodafone.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
	
@Service
public class SOAPMessageReader {
	Logger log = LoggerFactory.getLogger(SOAPMessageReader.class);
	public String findSOAPField(Document doc, String elementName)throws Exception{
		String response = "";
		try{
		  
		  NodeList list = doc.getElementsByTagName(elementName);
		  if(list.getLength() > 0){
			  Node node = list.item(0);
			  response = node.getTextContent();
		  }else{
			  
		  }
		}catch(Exception e){
			
		}finally{
//			in.close();
		}
		return response;
	}

	public String findSOAPField(String request, String elementName) throws Exception{
		InputStream in = new ByteArrayInputStream(request.getBytes());

		DocumentBuilderFactory docFactory = null;  
		DocumentBuilder docBuilder = null;  
		Document doc = null;  
		docFactory = DocumentBuilderFactory.newInstance();  
		docBuilder = docFactory.newDocumentBuilder();  
		doc = docBuilder.parse(in); 
		String response = "";
		
		try{		
			NodeList list = doc.getElementsByTagName(elementName);
			if(list.getLength() > 0){
				Node node = list.item(0);
				response = node.getTextContent();
			}else{
				
			}
		}catch(Exception e){
			
		}finally{
//			in.close();
		}
		return response;
	}
	   
}
