package com.itconsortiumgh.vodafone.model;

import com.itconsortiumgh.vodafone.db.Merchant;

import lombok.Data;

@Data
public class CRUDResponse {
	private String responseCode;
	private String responseMessage;
	private Merchant merchant;
}
