package com.itconsortiumgh.vodafone.model;

import lombok.Data;

@Data
public class ExternalCallback {
	private String refNo;
	private String responseCode;
	private String responseMessage;
	
}
