package com.itconsortiumgh.vodafone.model;

import java.util.List;

import com.itconsortiumgh.vodafone.db.TransactionLogs;

import lombok.Data;

@Data
public class TransactionListResponse {
	private String responseCode;
	private String responseMessage;
	private List<TransactionLogs> transactionLogsList;
}
