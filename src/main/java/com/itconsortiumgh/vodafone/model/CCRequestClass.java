package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class CCRequestClass {
	private String refNo;
	private String msisdn;
	private String amount;
	private String apiKey;
}
