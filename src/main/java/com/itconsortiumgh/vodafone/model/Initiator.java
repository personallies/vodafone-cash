package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Initiator {
	private String identifierType;
	private String identifer;
	private String securityCredential;
	private String shortCode;
	

}
