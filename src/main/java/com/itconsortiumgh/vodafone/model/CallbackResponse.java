package com.itconsortiumgh.vodafone.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@XmlRootElement(name="CallbackResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CallbackResponse {
	@XmlElement(name="ResponseCode")
	private String responseCode;
	@XmlElement(name="ResponseMessage")
	private String responseMessage;
//	private String refNo;

}
