package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ResponseMessage {
	public static final String PAYMENT_PROCESS = "Processing payment";
	public static final String PAYMENT_SUCCESS = "Payment successful";
	public static final String PAYMENT_FAIL = "Payment failed";
	public static final String MERCHANT_CREATE_SUCCESS = "Merchant creation successful";
	public static final String MERCHANT_CREATE_FAIL = "Merchant creation failure";
	public static final String MERCHANT_RETRIEVE_SUCCESS = "Merchant retrieval successful";
	public static final String MERCHANT_RETRIEVE_FAIL = "Merchant retrieval failure";
	public static final String MERCHANT_UPDATE_SUCCESS = "Merchant update successful";
	public static final String MERCHANT_UPDATE_FAIL = "Merchant update failure";
	public static final String MERCHANT_DELETE_SUCCESS = "Merchant deletion successful";
	public static final String MERCHANT_DELETE_FAIL = "Merchant deletion failure";
	public static final String TRANS_RETRIEVE_SUCCESS = "Transactions retrieval successful";
	public static final String TRANS_RETRIEVE_FAIL = "Transactions retrieval failure";
	public static final String INVALID_REQUEST = "Invalid Request";
	public static final String SERVER_FAIL = "Connection is closed";
	public static final String NOT_FOUND = "Record not found";
	public static final String DATA_VIOLATION = "Data Integrity Violation";
}
