package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Caller {
	private String callerType ;
	private String thirdPartyID;
	private String password;
	private String resultUrl;
}
