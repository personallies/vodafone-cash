package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ReceiverParty {
	private String identifierType;
	private String identifier;
	
}
