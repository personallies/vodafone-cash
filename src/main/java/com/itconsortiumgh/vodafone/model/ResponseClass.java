package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Component
public class ResponseClass {
	private String responseCode;
	private String responseMessage;
//	private String originatorConversationID;
	@JsonIgnore
	private String conversationID;
	@JsonIgnore
	private String serviceStatus;

}
