package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Transaction {
	private String commandID;
	private Parameters parameters;
	private ReferenceData referenceData;
//	private String languageCode;
//	private String originatorConversationID;
//	private String conversationID;
//	private String remark;
//	private String encryptedParameters;
//	private String timeStamp;
}
