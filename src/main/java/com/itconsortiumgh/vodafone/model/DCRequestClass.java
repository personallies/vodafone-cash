package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class DCRequestClass {
	private String refNo;
	private String msisdn;
	private String amount;
	private String voucher;
	private String apiKey;
}
