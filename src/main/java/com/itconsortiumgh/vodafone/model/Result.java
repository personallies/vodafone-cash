package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Result {
	private String resultType;
	private String resultCode;
	private String resultDesc;
	private String originatorConversationID;
	private String conversationID;
	private String transactionID;
	private String resultParameters;//may be of type of Parameter
//	private ReferenceItem referenceData;
}
