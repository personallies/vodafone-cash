package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Identity {
	private Caller caller;
	private Initiator initiator;
	private ReceiverParty receiverParty;
}
