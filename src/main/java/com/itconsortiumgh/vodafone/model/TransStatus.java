package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TransStatus {
	public static final String CREATED="Created";
	public static final String UPDATED="Updated";
	public static final String COMPLETED="Completed";
	public static final String SUCCESS = "Successful";
	public static final String FAIL = "Failure";

}
