package com.itconsortiumgh.vodafone.model;

import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Parameters {
	private List<Parameter> parameterList;

}
