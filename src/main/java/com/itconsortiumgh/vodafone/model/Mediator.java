package com.itconsortiumgh.vodafone.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Mediator {
	private String callerType ;
	private String thirdPartyID;
	private String password;
	private String resultUrl;
	
	private String iIdentifierType;
	private String iIdentifier;
	private String securityCredential;
	private String shortCode;
	
	private String rIdentifierType;
	private String rIdentifier;
	
	private String commandId;
}
