package com.itconsortiumgh.vodafone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.vodafone.db.Merchant;
import com.itconsortiumgh.vodafone.db.TransactionLogs;
import com.itconsortiumgh.vodafone.model.CCRequestClass;
import com.itconsortiumgh.vodafone.model.CRUDResponse;
import com.itconsortiumgh.vodafone.model.CallbackResponse;
import com.itconsortiumgh.vodafone.model.DCRequestClass;
import com.itconsortiumgh.vodafone.model.ExternalCallback;
import com.itconsortiumgh.vodafone.model.ResponseClass;
import com.itconsortiumgh.vodafone.model.ResponseCode;
import com.itconsortiumgh.vodafone.model.ResponseMessage;
import com.itconsortiumgh.vodafone.model.TransactionListResponse;
import com.itconsortiumgh.vodafone.repository.MerchantTableRepository;
import com.itconsortiumgh.vodafone.repository.TransLogsRepository;
import com.itconsortiumgh.vodafone.service.BusinessLogic;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class Controller {
	@Autowired
	BusinessLogic businessLogic;
	@Autowired
	MerchantTableRepository merchantTableRepository;
	@Autowired
	TransLogsRepository transLogsRepository;
	
	@PostMapping("create/merchant")
	public CRUDResponse createMerchant(@RequestBody Merchant merchant){
		log.info("************The Json request : {}",merchant);
		CRUDResponse crudResponse = new CRUDResponse();
		try {
			if(merchant != null){
				Merchant savedMerchant = merchantTableRepository.save(merchant);
				if(savedMerchant != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_CREATE_SUCCESS);
					crudResponse.setMerchant(savedMerchant);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_CREATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_CREATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
				crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			crudResponse.setResponseCode(ResponseCode.DATA_VIOLATION);
			crudResponse.setResponseMessage(ResponseMessage.DATA_VIOLATION);
		}
		return crudResponse;
	}
	
	@GetMapping("retrieve/merchant/id/{id}")
	public CRUDResponse retrieveMerchant(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		Merchant foundMerchant = merchantTableRepository.findByMerchantId(id);
		if(foundMerchant != null){
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.MERCHANT_RETRIEVE_SUCCESS);
			crudResponse.setMerchant(foundMerchant);;
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	
	@PostMapping("update/merchant")
	public CRUDResponse updateMerchant(@RequestBody Merchant merchant){
		log.info("************The Json request : {}",merchant);
		CRUDResponse crudResponse = new CRUDResponse();
		if(merchant != null){
			Merchant foundMerchant = merchantTableRepository.findByMerchantId(merchant.getMerchantId());
			if(foundMerchant != null){
				if(merchant.getMerchantLegalName() != null)
					foundMerchant.setMerchantLegalName(merchant.getMerchantLegalName());
				if(merchant.getMerchantShortName() != null)
					foundMerchant.setMerchantShortName(merchant.getMerchantShortName());
				if(merchant.getMerchantCategory() != null)
					foundMerchant.setMerchantCategory(merchant.getMerchantCategory());
				if(merchant.getAddress() != null)
					foundMerchant.setAddress(merchant.getAddress());
				if(merchant.getSupportEmail() != null)
					foundMerchant.setSupportEmail(merchant.getSupportEmail());
				if(merchant.getSupportMobile() != null)
					foundMerchant.setSupportMobile(merchant.getSupportMobile());
				if(merchant.getCallbackUrl() != null)
					foundMerchant.setCallbackUrl(merchant.getCallbackUrl());
				if(merchant.getActive() != null)
					foundMerchant.setActive(merchant.getActive());
				if(merchant.getApiKey() != null)
					foundMerchant.setApiKey(merchant.getApiKey());
				if(merchant.getOva() != null)
					foundMerchant.setOva(merchant.getOva());
				Merchant savedMerchant = merchantTableRepository.save(foundMerchant);
				if(savedMerchant != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_UPDATE_SUCCESS);
					crudResponse.setMerchant(savedMerchant);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_UPDATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_UPDATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
				crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
			}
		}else{
			crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
			crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return crudResponse;
	}
	
	@GetMapping("delete/merchant/id/{id}")
	public CRUDResponse deleteMerchant(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		Merchant foundMerchant = merchantTableRepository.findByMerchantId(id);
		if(foundMerchant != null){
			merchantTableRepository.delete(foundMerchant);
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.MERCHANT_DELETE_SUCCESS);
			crudResponse.setMerchant(foundMerchant);
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	
	@GetMapping("retrieve/transactions/merchant/id/{id}")
	public TransactionListResponse retrieveTransactions(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		TransactionListResponse transactionListResponse = new TransactionListResponse();
		List<TransactionLogs> foundTransactionLogs = transLogsRepository.findByMerchantId(id);
		if(foundTransactionLogs != null){
			transactionListResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			transactionListResponse.setResponseMessage(ResponseMessage.TRANS_RETRIEVE_SUCCESS);
			transactionListResponse.setTransactionLogsList(foundTransactionLogs);
		}else{
			transactionListResponse.setResponseCode(ResponseCode.TRANS_RETRIEVE_FAIL);
			transactionListResponse.setResponseMessage(ResponseMessage.TRANS_RETRIEVE_FAIL);
		}
		return transactionListResponse;
	}
	
	@PostMapping("/credit/customer")
	public ResponseClass creditCustomer(@RequestBody CCRequestClass requestClass){
		log.info("************The Json request : {}",requestClass);
		ResponseClass responseClass = new ResponseClass();
		if(requestClass != null){
			String apiKey = requestClass.getApiKey();
			Merchant foundMerchant = merchantTableRepository.findByApiKey(apiKey);
			
			if(foundMerchant != null){
				responseClass = businessLogic.creditCustomer(requestClass);
			}else{
				responseClass.setResponseCode(ResponseCode.NOT_FOUND);
				responseClass.setResponseMessage(ResponseMessage.NOT_FOUND);
			}
		}else{
			responseClass.setResponseCode(ResponseCode.INVALID_REQUEST);
			responseClass.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return responseClass;
	}
	
	@PostMapping("/debit/customer")
	public ResponseClass debitCustomer(@RequestBody DCRequestClass requestClass){
		log.info("************The Json request : {}",requestClass);
		ResponseClass responseClass = new ResponseClass();
		if(requestClass != null){
			String apiKey = requestClass.getApiKey();
			Merchant foundMerchant = merchantTableRepository.findByApiKey(apiKey);
			
			if(foundMerchant != null){
				responseClass = businessLogic.debitCustomer(requestClass);
			}else{
				responseClass.setResponseCode(ResponseCode.NOT_FOUND);
				responseClass.setResponseMessage(ResponseMessage.NOT_FOUND);
			}	
		}else{
			responseClass.setResponseCode(ResponseCode.INVALID_REQUEST);
			responseClass.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return responseClass;
	}
	
	@PostMapping(value="/callback/result" , produces = "application/xml")
	public ExternalCallback callbackCreditCustomer(@RequestBody String requestXml){
		System.out.println("************The Xml request : \n"+requestXml);
		
		int indexOfResult = requestXml.indexOf("<Result>");
		int indexOfResultEnd = requestXml.indexOf("</Result>");
		String result = requestXml.substring(indexOfResult, indexOfResultEnd);
	
		return businessLogic.callbackCreditCustomer(result+"</Result>");
	}
	
	@PostMapping("/callback/local")
	public CallbackResponse callbackLocal(@RequestBody CallbackResponse callBackResponse){
		log.info("************The Json request : {}", callBackResponse);
		return callBackResponse;
	}
}
