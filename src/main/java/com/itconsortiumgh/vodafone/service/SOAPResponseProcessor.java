package com.itconsortiumgh.vodafone.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.itconsortiumgh.vodafone.model.ResponseClass;
import com.itconsortiumgh.vodafone.utils.SOAPMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SOAPResponseProcessor {
	@Autowired
	SOAPMessageReader soapMessageReader;

	public ResponseClass produceResponse(String requestXml){
		log.info("********Processing the response Xml*********");
		ResponseClass response = new ResponseClass();
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(requestXml)));
			
			document.getDocumentElement().normalize();
			
			NodeList responses = document.getElementsByTagName("Response");
			for(int i=0; i<responses.getLength(); i++){
				Node firstServiceNode = responses.item(i);
				
				if(firstServiceNode.getNodeType() == Node.ELEMENT_NODE){
					Element element = (Element) firstServiceNode;
					
					NodeList responseCodeList = element.getElementsByTagName("ResponseCode");
					Element responseCodeElement = (Element) responseCodeList.item(0);
					String responseCode = responseCodeElement.getTextContent();
					
					NodeList responseDescList = element.getElementsByTagName("ResponseDesc");
					Element responseDescElement = (Element) responseDescList.item(1);
					String responseDesc = responseDescElement.getTextContent();
					
//					NodeList originatorConversationIDList = element.getElementsByTagName("OriginatorConversationID");
//					Element originatorConversationIDElement = (Element) originatorConversationIDList.item(2);
//					String originatorConversationID = originatorConversationIDElement.getTextContent();
					
					NodeList conversationIDList = element.getElementsByTagName("ConversationID");
					Element conversationIDElement = (Element) conversationIDList.item(2);
					String conversationID = conversationIDElement.getTextContent();
					
					NodeList serviceStatusList = element.getElementsByTagName("ServiceStatus");
					Element serviceStatusElement = (Element) serviceStatusList.item(3);
					String serviceStatus = serviceStatusElement.getTextContent();
					
					response.setResponseCode(responseCode);
					response.setResponseMessage(responseDesc);
//					response.setOriginatorConversationID(originatorConversationID);
					response.setConversationID(conversationID);
					response.setServiceStatus(serviceStatus);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}
	
	public ResponseClass processResponse(String requestXml){
		ResponseClass responseClass = new ResponseClass();
		
		String responseCode = "";
//		String originatorConversationID = "";
		String conversationID = "";
		String responseDesc = ""; 
		String serviceStatus = "";
		
		
		try {
			responseCode = soapMessageReader.findSOAPField(requestXml, "ResponseCode");
//			originatorConversationID = soapMessageReader.findSOAPField(requestXml, "OriginatorConversationID");
			conversationID = soapMessageReader.findSOAPField(requestXml, "ConversationID");
			responseDesc = soapMessageReader.findSOAPField(requestXml, "ResponseDesc");
			serviceStatus = soapMessageReader.findSOAPField(requestXml, "ServiceStatus");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		responseClass.setResponseCode(responseCode);
//		responseClass.setOriginatorConversationID(originatorConversationID);
		responseClass.setConversationID(conversationID);
		responseClass.setResponseMessage(responseDesc);
		responseClass.setServiceStatus(serviceStatus);
		
		return responseClass;
	}
}
