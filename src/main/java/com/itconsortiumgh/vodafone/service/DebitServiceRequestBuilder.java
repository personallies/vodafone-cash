package com.itconsortiumgh.vodafone.service;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.itconsortiumgh.vodafone.model.Mediator;
import com.itconsortiumgh.vodafone.model.Parameter;
import com.itconsortiumgh.vodafone.model.Parameters;
import com.itconsortiumgh.vodafone.model.ReferenceData;
import com.itconsortiumgh.vodafone.model.Transaction;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DebitServiceRequestBuilder {
	
//	public static void main(String[] args) {
//		DebitServiceRequestBuilder builder = new DebitServiceRequestBuilder();
//		builder.request(null, "kel", "kel", "kel","kel","kel","kel","kel","kel","kel","kel","kel","kel","kel");
//	}

	public String header(String requestMessageXml){
		log.info("********Processing the request Xml*********");
		String requestXml = "";
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();
			
			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:req", "http://cps.huawei.com/cpsinterface/request");
			message.appendChild(envelope);
			
            Element header = message.createElement("soapenv:Header");
            envelope.appendChild(header);
            
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element requestMsg = message.createElement("req:RequestMsg");
			StringBuilder sb = new StringBuilder();
			sb.append("<![CDATA[");
			sb.append(StringEscapeUtils.unescapeHtml4(requestMessageXml));
			sb.append("]]>");
			requestMsg.setTextContent(sb.toString());
			body.appendChild(requestMsg);
			
            DOMSource domSource = new DOMSource(message);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
            transformer.setOutputProperty
                    ("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            requestXml = sw.toString();

		}catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
		System.out.println(requestXml.replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
		return requestXml.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
		
	}
	public String request(Mediator mediator, Parameters parametersBody, String vTimeStamp, String vKeyOwner){
		log.info("********Processing the request Xml*********");
		String requestXml = "";
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();
			
			Document message = impl.createDocument(null, null, null);
			
			Element request = message.createElement("Request");
			message.appendChild(request);
			
			Element identity = message.createElement("Identity");
			request.appendChild(identity);
			
			Element caller = message.createElement("Caller");
			identity.appendChild(caller);
			
			Element callerType =  message.createElement("CallerType");
			callerType.setTextContent(mediator.getCallerType());
			caller.appendChild(callerType);
			
			Element thirdPartyId = message.createElement("ThirdPartyID");
			thirdPartyId.setTextContent(mediator.getThirdPartyID());
			caller.appendChild(thirdPartyId);
			
			Element password = message.createElement("Password");
			password.setTextContent(mediator.getPassword());
			caller.appendChild(password);
			
			Element resultURL = message.createElement("ResultURL");
			resultURL.setTextContent(mediator.getResultUrl());
			caller.appendChild(resultURL);
			
			Element initiator = message.createElement("Initiator");
			identity.appendChild(initiator);
			
			Element identifierType = message.createElement("IdentifierType");
			identifierType.setTextContent(mediator.getIIdentifierType());
			initiator.appendChild(identifierType);
			
			Element identifier = message.createElement("Identifier");
			identifier.setTextContent(mediator.getIIdentifier());
			initiator.appendChild(identifier);
			
			Element securityCredential = message.createElement("SecurityCredential");
			securityCredential.setTextContent(mediator.getSecurityCredential());
			initiator.appendChild(securityCredential);
			
			Element shortCode = message.createElement("ShortCode");
			shortCode.setTextContent(mediator.getShortCode());
			initiator.appendChild(shortCode);
			
			Element receiverParty = message.createElement("ReceiverParty");
			identity.appendChild(receiverParty);
			
			Element identifierType2 =message.createElement("IdentifierType");
			identifierType2.setTextContent(mediator.getRIdentifierType());
			receiverParty.appendChild(identifierType2);
			
			Element identifier2 = message.createElement("Identifier");
			identifier2.setTextContent(mediator.getRIdentifier());
			receiverParty.appendChild(identifier2);
			
			Element transaction = message.createElement("Transaction");
			request.appendChild(transaction);
			
			Element commandID = message.createElement("CommandID");
			commandID.setTextContent(mediator.getCommandId());
			transaction.appendChild(commandID);
			
			Element parameters = message.createElement("Parameters");
			transaction.appendChild(parameters);

			for(Parameter parameterList : parametersBody.getParameterList()){
				Element parameter = message.createElement("Parameter");
				parameters.appendChild(parameter);
				
				Element key = message.createElement("Key");
				key.setTextContent(parameterList.getKey());
				parameter.appendChild(key);
				
				Element value = message.createElement("Value");
				value.setTextContent(parameterList.getValue());
				parameter.appendChild(value);
			}
			
			Element timeStamp = message.createElement("Timestamp");
			timeStamp.setTextContent(vTimeStamp);
			transaction.appendChild(timeStamp);

			Element keyOwner = message.createElement("KeyOwner");
			keyOwner.setTextContent(vKeyOwner);
			request.appendChild(keyOwner);
			
	
            DOMSource domSource = new DOMSource(message);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
            transformer.setOutputProperty
                    ("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            requestXml = sw.toString();
			
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
		return requestXml;
	}
}
