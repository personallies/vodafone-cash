package com.itconsortiumgh.vodafone.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.itconsortiumgh.vodafone.model.Result;
import com.itconsortiumgh.vodafone.utils.SOAPMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SOAPResultProcessor {
	@Autowired
	SOAPMessageReader soapMessageReader;

	public Result produceResult(String requestXml){
		log.info("********Processing result xml*********");
		Result result = new Result();
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(requestXml)));
			
			document.getDocumentElement().normalize();
			
			NodeList results = document.getElementsByTagName("Result");
			for(int i=0; i<results.getLength(); i++){
				Node firstServiceNode = results.item(i);
				
				if(firstServiceNode.getNodeType() == Node.ELEMENT_NODE){
					Element element = (Element) firstServiceNode;
					
					NodeList resultTypeList = element.getElementsByTagName("ResultType");
					Element resultTypeElement = (Element) resultTypeList.item(0);
					String resultType = resultTypeElement.getTextContent();
					
					NodeList resultCodeList = element.getElementsByTagName("ResultCode");
					Element resultCodeElement = (Element) resultCodeList.item(1);
					String resultCode = resultCodeElement.getTextContent();
					
					NodeList resultDescList = element.getElementsByTagName("ResultDesc");
					Element resultDescElement = (Element) resultDescList.item(2);
					String resultDesc = resultDescElement.getTextContent();
					
					NodeList originatorConversationIDList = element.getElementsByTagName("OriginatorConversationID");
					Element originatorConversationIDElement = (Element) originatorConversationIDList.item(3);
					String originatorConversationID = originatorConversationIDElement.getTextContent();
					
					NodeList conversationIDList = element.getElementsByTagName("ConversationID");
					Element conversationIDElement = (Element) conversationIDList.item(4);
					String conversationID = conversationIDElement.getTextContent();
					
					NodeList transactionIDList = element.getElementsByTagName("TransactionID");
					Element transactionIDElement = (Element) transactionIDList.item(5);
					String transactionID = transactionIDElement.getTextContent();
					
					NodeList resultParametersList = element.getElementsByTagName("ResultParameters");
					Element resultParametersElement = (Element) resultParametersList.item(6);
					String resultParameters = resultParametersElement.getTextContent();
				
					result.setResultType(resultType);
					result.setResultCode(resultCode);
					result.setResultDesc(resultDesc);
					result.setOriginatorConversationID(originatorConversationID);
					result.setConversationID(conversationID);
					result.setTransactionID(transactionID);
					result.setResultParameters(resultParameters);
					
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	public Result processResult(String requestXml){
		log.info("********Processing result xml*********");

		Result result = new Result();
		String resultType = "";
		String resultCode = ""; 
		String resultDesc = "";
		String originatorConversationID = "";
		String conversationID = "";
		String transactionID = "";
		String resultParameters = "";
		
		
		try {
			resultType = soapMessageReader.findSOAPField(requestXml, "ResultType");
			resultCode = soapMessageReader.findSOAPField(requestXml, "ResultCode");
			resultDesc = soapMessageReader.findSOAPField(requestXml, "ResultDesc");
			originatorConversationID = soapMessageReader.findSOAPField(requestXml, "OriginatorConversationID");
			conversationID = soapMessageReader.findSOAPField(requestXml, "ConversationID");
			transactionID = soapMessageReader.findSOAPField(requestXml, "TransactionID");
			resultParameters = soapMessageReader.findSOAPField(requestXml, "ResultParameters");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.setResultType(resultType);
		result.setResultCode(resultCode);
		result.setResultDesc(resultDesc);
		result.setOriginatorConversationID(originatorConversationID);
		result.setConversationID(conversationID);
		result.setTransactionID(transactionID);
		result.setResultParameters(resultParameters);
		return result;
	}
}
