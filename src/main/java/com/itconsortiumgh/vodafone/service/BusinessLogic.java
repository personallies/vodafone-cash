package com.itconsortiumgh.vodafone.service;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.vodafone.db.Merchant;
import com.itconsortiumgh.vodafone.db.TransactionLogs;
import com.itconsortiumgh.vodafone.model.Action;
import com.itconsortiumgh.vodafone.model.CCRequestClass;
import com.itconsortiumgh.vodafone.model.DCRequestClass;
import com.itconsortiumgh.vodafone.model.ExternalCallback;
import com.itconsortiumgh.vodafone.model.Mediator;
import com.itconsortiumgh.vodafone.model.Parameter;
import com.itconsortiumgh.vodafone.model.Parameters;
import com.itconsortiumgh.vodafone.model.ReferenceData;
import com.itconsortiumgh.vodafone.model.ReferenceItem;
import com.itconsortiumgh.vodafone.model.ResponseClass;
import com.itconsortiumgh.vodafone.model.ResponseCode;
import com.itconsortiumgh.vodafone.model.ResponseMessage;
import com.itconsortiumgh.vodafone.model.Result;
import com.itconsortiumgh.vodafone.model.TransStatus;
import com.itconsortiumgh.vodafone.properties.ApplicationProperties;
import com.itconsortiumgh.vodafone.repository.MerchantTableRepository;
import com.itconsortiumgh.vodafone.repository.TransLogsRepository;
import com.itconsortiumgh.vodafone.utils.SOAPMessagePoster;
import com.itconsortiumgh.vodafone.utils.TransactionIdGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BusinessLogic {
	@Autowired
	CreditServiceRequestBuilder creditServiceRequestBuilder;
	@Autowired
	DebitServiceRequestBuilder debitServiceRequestBuilder;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	SOAPMessagePoster soapMessagePoster;
	@Autowired
	SOAPResponseProcessor soapResponseProcessor;
	@Autowired
	SOAPResultProcessor soapResultProcessor;
	@Autowired
	TransLogsRepository transLogsRepository;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MerchantTableRepository merchantTableRepository;
	
	public ResponseClass creditCustomer(CCRequestClass requestClass){
		Mediator mediator = new Mediator();
		TransactionLogs transactionLogs = new TransactionLogs();
		ResponseClass responseClass = new ResponseClass();
		
		mediator.setCallerType(applicationProperties.getCallerType());
		mediator.setThirdPartyID(applicationProperties.getThirdPartyId());
		mediator.setPassword(applicationProperties.getPassword());
		mediator.setResultUrl(applicationProperties.getCallbackUrl());
		mediator.setIIdentifierType(applicationProperties.getIIdentifierType());
		mediator.setIIdentifier(applicationProperties.getOpId());
		String securityCredential = applicationProperties.getSecurityCredential();

		mediator.setSecurityCredential(securityCredential);
		mediator.setShortCode(applicationProperties.getOpCode());
		mediator.setRIdentifierType(applicationProperties.getCRIdentifierType());
		mediator.setRIdentifier(requestClass.getMsisdn());
		mediator.setCommandId(applicationProperties.getCredCustCommandId());
		
		//Parameters
		Parameters parameters = new Parameters();
		List<Parameter> tmpParameterList = new ArrayList<>();
		
		Parameter parameter1 = new Parameter();
		parameter1.setKey(applicationProperties.getAmount());
		parameter1.setValue(requestClass.getAmount());
		tmpParameterList.add(parameter1);
		
		Parameter parameter2 = new Parameter();
		parameter2.setKey(applicationProperties.getCurrency());
		parameter2.setValue(applicationProperties.getCurrencyVal());
		tmpParameterList.add(parameter2);
		
		parameters.setParameterList(tmpParameterList);;
		
		//ReferenceData
		ReferenceData referenceData = new ReferenceData();
		List<ReferenceItem> tmpReferenceItemList = new ArrayList<>();
		
		ReferenceItem referenceItem = new ReferenceItem();
		referenceItem.setKey(applicationProperties.getPosDeviceId());
		referenceItem.setValue(applicationProperties.getPosDeviceIdVal());
		tmpReferenceItemList.add(referenceItem);
		referenceData.setReferenceItemList(tmpReferenceItemList);
		
		//TimeStamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date now = new Date();
		String myDate = dateFormat.format(now);
 		String vTimeStamp = myDate;
 		
 		//KeyOwner
 		String vKeyOwner = applicationProperties.getKey();
		
 		try {
 			log.info("********Going to process the request Xml**********");
 			String requestXml = creditServiceRequestBuilder.request(mediator, parameters, referenceData, vTimeStamp, vKeyOwner);
 			String headerXml = creditServiceRequestBuilder.header(requestXml); 		
 			log.info("********Hitting {} for a response**********",applicationProperties.getUrl());
 			String responseXml = soapMessagePoster.sendMessage(applicationProperties.getUrl(), headerXml);
 			log.info("************The response :{}",responseXml);
 			
 			int indexOfResponseStart = responseXml.indexOf("<Response>");
 			int indexOfResponseEnd = responseXml.indexOf("</Response>");
 			
 			String responseStr = responseXml.substring(indexOfResponseStart, indexOfResponseEnd);
 			
 			log.info("********Going to process the response Xml**********");
 			responseClass = soapResponseProcessor.processResponse(responseStr+"</Response>");
 			log.info("************The final response :{}",responseClass);
 			
 			if(ResponseCode.VODAFONE_SUCCESS.equalsIgnoreCase(responseClass.getResponseCode())){
 				responseClass.setResponseCode(ResponseCode.GENERAL_SUCCESS);
 				responseClass.setResponseMessage(ResponseMessage.PAYMENT_PROCESS);
 			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
 			responseClass.setResponseCode(ResponseCode.SERVER_FAIL);
 			responseClass.setResponseMessage(ResponseMessage.SERVER_FAIL);
// 			return responseClass;
		}
		
		try {
			transactionLogs.setTransflowTransactionId(TransactionIdGenerator.nextId());
			transactionLogs.setReferenceNumber(requestClass.getRefNo());
			transactionLogs.setMsisdn(requestClass.getMsisdn());
			transactionLogs.setAmount(new BigDecimal(requestClass.getAmount()));
			transactionLogs.setTransactionType(Action.CREDIT);
			transactionLogs.setResultCode(responseClass.getResponseCode());
			transactionLogs.setResultDescription(responseClass.getResponseMessage());
			transactionLogs.setConversationId(responseClass.getConversationID());
//			transactionLogs.setServiceStatus(responseClass.getServiceStatus());
			transactionLogs.setTransactionStatus(TransStatus.CREATED);
			transactionLogs.setDateCreated(new Date());
			transactionLogs.setApiKey(requestClass.getApiKey());
			transLogsRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("************Logging failed");
		}
		
		
		return responseClass;
	}
	
	public ResponseClass debitCustomer(DCRequestClass requestClass){
		Mediator mediator = new Mediator();
		TransactionLogs transactionLogs = new TransactionLogs();
		ResponseClass responseClass = new ResponseClass();
		
		//Get last four digits of msisdn
		String inMsisdn = requestClass.getMsisdn();
		String outMsisdn = "";
		if(inMsisdn.length() == 12){
			outMsisdn = inMsisdn.substring(8, 12);
		}else if(inMsisdn.length() == 10){
			outMsisdn = inMsisdn.substring(6, 10);
		}else if(inMsisdn.length() == 13){
			outMsisdn = inMsisdn.substring(9, 13);
		}
		log.info("************The full Msisdn: {}",inMsisdn);
		log.info("************The truncated Msisdn: {}",outMsisdn);
		
		
		mediator.setCallerType(applicationProperties.getCallerType());
		mediator.setThirdPartyID(applicationProperties.getThirdPartyId());
		mediator.setPassword(applicationProperties.getPassword());
		mediator.setResultUrl(applicationProperties.getCallbackUrl());
		mediator.setIIdentifierType(applicationProperties.getIIdentifierType());
		mediator.setIIdentifier(applicationProperties.getOpId());
		String securityCredential = applicationProperties.getSecurityCredential();

		mediator.setSecurityCredential(securityCredential);
		mediator.setShortCode(applicationProperties.getOpCode());
		mediator.setRIdentifierType(applicationProperties.getDRIdentifierType());

		mediator.setRIdentifier(requestClass.getVoucher());
		mediator.setCommandId(applicationProperties.getDebtCustCommandId());
		
		//Parameters
		Parameters parameters = new Parameters();
		List<Parameter> tmpParameterList = new ArrayList<>();
		
		Parameter parameter1 = new Parameter();
		parameter1.setKey(applicationProperties.getVoucher());
		parameter1.setValue(outMsisdn);
		tmpParameterList.add(parameter1);
		
		Parameter parameter2 = new Parameter();
		parameter2.setKey(applicationProperties.getCurrency());
		parameter2.setValue(applicationProperties.getCurrencyVal());
		tmpParameterList.add(parameter2);
		
		Parameter parameter3 = new Parameter();
		parameter3.setKey(applicationProperties.getAmount());
		parameter3.setValue(requestClass.getAmount());
		tmpParameterList.add(parameter3);
		
		parameters.setParameterList(tmpParameterList);
		
		//TimeStamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date now = new Date();
		String myDate = dateFormat.format(now);
 		String vTimeStamp = myDate;
 		
 		//KeyOwner
 		String vKeyOwner = applicationProperties.getKey();
		 
 		try{
			log.info("********Going to process the request Xml**********");
			String requestXml = debitServiceRequestBuilder.request(mediator, parameters, vTimeStamp, vKeyOwner);
			String headerXml = debitServiceRequestBuilder.header(requestXml); 		
			log.info("********Hitting {} for a response**********",applicationProperties.getUrl());
			String responseXml = soapMessagePoster.sendMessage(applicationProperties.getUrl(), headerXml);
			log.info("************The response :{}",responseXml);
			
			int indexOfResponseStart = responseXml.indexOf("<Response>");
			int indexOfResponseEnd = responseXml.indexOf("</Response>");
		
			String responseStr = responseXml.substring(indexOfResponseStart, indexOfResponseEnd);
			
			log.info("********Going to process the response Xml**********");
			responseClass = soapResponseProcessor.processResponse(responseStr+"</Response>");
			log.info("************The final response :{}",responseClass);
			
 			if(ResponseCode.VODAFONE_SUCCESS.equalsIgnoreCase(responseClass.getResponseCode())){
 				responseClass.setResponseCode(ResponseCode.GENERAL_SUCCESS);
 				responseClass.setResponseMessage(ResponseMessage.PAYMENT_PROCESS);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 			responseClass.setResponseCode(ResponseCode.SERVER_FAIL);
 			responseClass.setResponseMessage(ResponseMessage.SERVER_FAIL);
// 			return responseClass;
 		}
		
		try {
			transactionLogs.setTransflowTransactionId(TransactionIdGenerator.nextId());
			transactionLogs.setReferenceNumber(requestClass.getRefNo());
			transactionLogs.setMsisdn(requestClass.getMsisdn());
			transactionLogs.setAmount(new BigDecimal(requestClass.getAmount()));
			transactionLogs.setTransactionType(Action.DEBIT);
			transactionLogs.setResultCode(responseClass.getResponseCode());
			transactionLogs.setResultDescription(responseClass.getResponseMessage());
			transactionLogs.setConversationId(responseClass.getConversationID());
//			transactionLogs.setServiceStatus(responseClass.getServiceStatus());
			transactionLogs.setTransactionStatus(TransStatus.CREATED);
			transactionLogs.setDateCreated(new Date());
			transactionLogs.setApiKey(requestClass.getApiKey());
			transLogsRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("************Logging failed");
		}
		
		return responseClass;
	}
	
	public ExternalCallback callbackCreditCustomer(String requestXml){
		ExternalCallback externalCallback= new ExternalCallback();
		log.info("********BusinessLogic*****");
		Result result = soapResultProcessor.processResult(requestXml);
		log.info("************The Json result:{} ",result);
		String callbackUrl =  "";
		try {
			if(result != null){
//				TransactionLogs foundTransactionLogs = transLogsRepository.findByTransactionId(result.getTransactionID());
				TransactionLogs foundTransactionLogs = transLogsRepository.findByConversationId(result.getConversationID());
				if(foundTransactionLogs != null){
					TransactionLogs transactionLogs = foundTransactionLogs;
//					transactionLogs.setType(result.getResultType());
					transactionLogs.setResultCode(result.getResultCode());
					transactionLogs.setResultDescription(result.getResultDesc());
//					transactionLogs.setOriginatorConversationId(result.getOriginatorConversationID());
					transactionLogs.setConversationId(result.getConversationID());
					transactionLogs.setTransactionId(result.getTransactionID());
//					transactionLogs.setResultParameters(result.getResultParameters());
					transactionLogs.setDateProcessed(new Date());

					
					if(ResponseCode.VODAFONE_SUCCESS.equalsIgnoreCase(result.getResultCode())){
						transactionLogs.setTransactionStatus(TransStatus.SUCCESS);
						externalCallback.setResponseCode(ResponseCode.GENERAL_SUCCESS);
						externalCallback.setResponseMessage(ResponseMessage.PAYMENT_SUCCESS);
					}else{
						transactionLogs.setTransactionStatus(TransStatus.FAIL);
						externalCallback.setResponseCode(result.getResultCode());
						externalCallback.setResponseMessage(result.getResultDesc());
//						externalCallback.setResponseCode(ResponseCode.PAYMENT_FAIL);
//						externalCallback.setResponseMessage(ResponseMessage.PAYMENT_FAIL);
					}
					
					externalCallback.setRefNo(transactionLogs.getReferenceNumber());
					Merchant foundMerchant = merchantTableRepository.findByApiKey(transactionLogs.getApiKey());
					//Call third party with callback response
					if(foundMerchant != null){
						transactionLogs.setMerchantId(foundMerchant.getMerchantId());
						callbackUrl = foundMerchant.getCallbackUrl();
					}else{
						externalCallback.setResponseCode(ResponseCode.NOT_FOUND);
						externalCallback.setResponseMessage(ResponseMessage.NOT_FOUND);
					}
					log.info("************The callback url{}",callbackUrl);
					transLogsRepository.save(transactionLogs);
					log.info("************The callback response : {}", externalCallback);
					String externalResponse = restTemplate.postForObject(callbackUrl, externalCallback, String.class);
					log.info("************The response from the external party : {}",externalResponse);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("************Logging failed");
		}

		return externalCallback;
	}
}
