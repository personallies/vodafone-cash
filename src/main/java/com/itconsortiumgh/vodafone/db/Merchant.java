package com.itconsortiumgh.vodafone.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Table(name="merchant_table", indexes=@Index(name="ova_index", columnList = "ova"))
@Entity
@Component
//@IdClass(MerchantId.class)
public class Merchant {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(length = 20, unique=true)
	private String merchantId;
	@Column(length = 100,unique=true)
	private String merchantLegalName;
	@Column(length = 50)
	private String merchantShortName;
	@Column
	private String address;
	@Column(length = 15)
	private String supportMobile;
	@Column(length = 100)
	private String supportEmail;
	@Column(length = 50)
	private String merchantCategory;
	@Column(length = 50, name = "ova", nullable=false)
	private String ova;
	@Column(length = 50, unique=true)
	private String apiKey;
	@Column
	private String callbackUrl;
	@Column(length=5)
	private String active;
}
