package com.itconsortiumgh.vodafone.db;



import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table
public class TransactionLogs {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(length = 50)
	private String transactionId;
	@Column(length = 50)
	private Long transflowTransactionId;
	@Column(length = 50)
	private String referenceNumber;
	@Column(length = 50)
	private String conversationId;
//	@Column(length = 50)
//	private String originatorConversationId;
//	@Column(length = 50)
//	private String type;
	@Column(length = 50)
	private String resultCode;
	@Column(length = 50)
	private String resultDescription;
//	@Column(length = 50)
//	private String resultParameters;
	@Column(length = 50)
	private String transactionStatus;
//	@Column(length = 50)
//	private String serviceStatus;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated;
	@Column 
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateProcessed;
	@Column(length = 50)
	private String transactionType;
	@Column(length = 50)
	private String msisdn;
	@Column(precision = 12, scale = 2)
	private BigDecimal amount;
	@Column(length = 20)
	private String merchantId;
	@Column(length = 50)
	private String apiKey;
}
