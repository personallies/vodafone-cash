package com.itconsortiumgh.vodafone.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.vodafone.db.TransactionLogs;

@Repository
public interface TransLogsRepository extends CrudRepository<TransactionLogs, Long>{
	TransactionLogs findByTransflowTransactionId(Long transflowTransactionId);
	TransactionLogs findByTransactionId(String transactionId);
	TransactionLogs findByConversationId(String conversationId);
	List<TransactionLogs> findByMerchantId(String merchantId);
}
