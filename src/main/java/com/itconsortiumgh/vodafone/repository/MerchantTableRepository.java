package com.itconsortiumgh.vodafone.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.vodafone.db.Merchant;



public interface MerchantTableRepository extends CrudRepository<Merchant, Long>{
	Merchant findByMerchantId(String merchantId);
	Merchant findByApiKey(String apiKey);
}
