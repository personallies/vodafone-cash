package com.itconsortiumgh.vodafone.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties
public class ApplicationProperties {
	private String url;
	
	private String callerType;
	private String thirdPartyId;
	private String password;
	private String callbackUrl;
	
	private String iIdentifierType;
	private String opId;
	private String pin;
	private String securityCredential;

	private String opCode;
	
	private String cRIdentifierType;
	private String credCustCommandId;
	
	private String dRIdentifierType;
	private String debtCustCommandId;

	private String voucher;
	private String amount;
	private String currency;
	private String currencyVal;
	
	private String posDeviceId;
	private String posDeviceIdVal;
	
	private String key;
}
