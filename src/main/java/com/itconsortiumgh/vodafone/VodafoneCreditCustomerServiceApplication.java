package com.itconsortiumgh.vodafone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableAutoConfiguration
public class VodafoneCreditCustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VodafoneCreditCustomerServiceApplication.class, args);
	}
	
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
